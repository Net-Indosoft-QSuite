#!perl -T
use 5.006;
use strict;
use warnings FATAL => 'all';
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'Net::Indosoft::QSuite' ) || print "Bail out!\n";
}

diag( "Testing Net::Indosoft::QSuite $Net::Indosoft::QSuite::VERSION, Perl $], $^X" );
